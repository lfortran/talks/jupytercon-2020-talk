# Title

LFortran: Interactive LLVM-based Fortran Compiler for Modern Architectures

# Authors

Ondřej Čertík, ondrej@certik.us, Los Alamos National Laboratory  
Milan Curcic, caomaco@gmail.com, University of Miami  

# Brief summary

We are developing a modern open-source Fortran compiler called LFortran (https://lfortran.org/). This compiler can compile Fortran source code to binaries as well as enable the interactive execution of code in environments like Jupyter. This will allow exploratory work (much like Python, MATLAB or Julia) which is currently not feasible with Fortran.

# Outline

# Abstract

We are developing a modern open-source Fortran compiler called LFortran
(https://lfortran.org/). This front-end compiler will enable the interactive
execution of code in environments like Jupyter. This will allow exploratory work
(much like Python, MATLAB or Julia) which is currently not feasible. The
interactivity of our compiler does not impede compilation of binaries with the
goal to run user's code on modern architectures such as multi-core CPUs and
GPUs, which is an essential requirement for wider Fortran adoption that current
Fortran compilers do not address well. A Live demo of the compiler with a
Jupyter notebook will be shown. The compiler itself is written in C++ for
robustness and speed with optional Python wrappers to improve inter-operability.
It parses Fortran code to an Abstract Syntax Tree (AST) and transforms it to an
Abstract Semantic Representation (ASR). LFortran has several backends that
transform the ASR to machine code via LLVM, or to C++, or to provide automatic
Python wrappers. More backends are planned. The compiler has been designed to be
modular so that data can be extracted/inserted between the different stages,
which is an important feature that would support an ecosystem of tools that
otherwise would be hard with a monolithic compiler.

# Background/Motivation

Fortran is widely used for High Performance Computing (HPC) codes in general
(e.g., according to the recent survey [5], Fortran is used in 7 out of the top
10 HPC codes). However, when doing exploratory rapid prototyping work, it is
common to choose Python or MATLAB due to their interactivity, but then one must
translate, say, from Python into Fortran in order to put the capability into
production codes. One way to solve this problem is to take Fortran, a high
performance language that is designed for array oriented scientific computing,
and try to make it interactive and that is the goal of LFortran.

## Methods

LFortran is written in C++ with optional Python wrappers and it has multiple
backends to generate code including LLVM [3] and C++. In addition, it can also
generate automatic wrappers of Fortran code into Python, C++ and other
languages. The compiler has been open sourced under the BSD license, available
at https://lfortran.org/ and the GitHub mirror already has 164
stars as of July 22, 2020. The Abstract Syntax Tree (AST) is represented
using the ASDL domain specific language [4], just like CPython's AST. LFortran
is designed as a library with separate building blocks (parser, AST, ASR,
semantic phase, codegen) that are all exposed to the user/developer in a
natural way to make it easy to contribute back.

# Results

We wrote a Fortran kernel to work in Jupyter, a demo notebook is available at
[1] (static) and [2] (binder). We will show live usage during the presentation.
Interactive plotting is supported via Matplotlib.

# Conclusions

Here are some of the ideas for tools that LFortran will enable the community to
build on top: optimization of loops; IDE support; language interoperability:
automatic wrappers to and from other languages (C, C++, Python, Julia, Matlab,
...); code refactoring; new real types: arbitrary precision floating point,
automatic differentiation type, interval arithmetics etc.; "Safe Mode": the
compiler will warn about "obsolete" usage, and will check arrays, pointers and
other things to never segfault in Debug mode; allow to restrict what Fortran
features are allowed for a given project; support for CUDA Fortran, OpenMP and
OpenACC; other backends such as C and Fortran that work with all major C and
Fortran compilers; better debugger (IPython.embed() for Fortran) and many
others. Use of SymPy/Symengine [6] to help rewrite symbolic Fortran
expressions, e.g., to better optimize them for double precision floating point
accuracy.

We hope that this project can make Fortran more available as a choice for many
users from the Python, Julia or MATLAB community.

The intended audience is any user from the Python, Julia or MATLAB scientific
or data community that wants to use the productive modern interactive
development workflow, but also wants the code to run fast using an established
language (Fortran) that looks very similar to Python/NumPy or MATLAB. Attendees
will learn that it is possible to use Fortran in a modern interactive way, and
the currently developped features of LFortran might be enough for some
attendees to start using it for their work.



[1] https://nbviewer.jupyter.org/gist/certik/f1d28a486510810d824869ab0c491b1c

[2] https://mybinder.org/v2/gl/lfortran%2Fweb%2Flfortran-binder/master?filepath=Demo.ipynb

[3] https://llvm.org/

[4] Wang, D. C., Appel, A. W., Korn, J. L., & Serra, C. S. (1997). The Zephyr
Abstract Syntax Description Language. USENIX Conference on Domain-Specific
Languages, (October), 213–228.

[5] HPC User Site Census. https://www.nvidia.com/content/intersect-360-HPC-application-support.pdf

[6] https://sympy.org/, https://github.com/symengine/symengine
