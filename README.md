# JupyterCon 2020 Talk

Abstract and presentation about LFortran for [JupyterCon 2020](https://jupytercon.com/).

* Abstract: [abstract.md](./abstract.md)
* Presentation slides: [certik.pdf](./certik.pdf)

Compile using:

    pdflatex certik.pdf
